package redis

import (
	"github.com/gomodule/redigo/redis"
	"sync"
	"time"
)

var once sync.Once
var defaultRedis *Redis

/*
Init 初始化默认Redis
  args:
   redisConfig IRedisConfig的实现
*/
func Init(redisConfig IRedisConfig) {
	once.Do(func() {
		defaultRedis = New(redisConfig)
	})

	return
}

/*
New 新建Redis指针
  args:
   redisConfig IRedisConfig的实现

  return:
   *Redis Redis对象指针
*/
func New(redisConfig IRedisConfig) *Redis {
	pool := &redis.Pool{
		// 超出最大连接数时是否等待, 不等待则报错
		Wait: redisConfig.GetWait(),
		// 最大闲置链接数
		MaxIdle: redisConfig.GetMaxIdle(),
		// 最大可用连接数
		MaxActive: redisConfig.GetMaxActive(),
		// 闲置超时时间
		IdleTimeout: time.Duration(redisConfig.GetIdleTimeout()) * time.Minute,
		// 链接配置
		Dial: func() (redis.Conn, error) {
			return redis.Dial(
				redisConfig.GetNetWork(),
				redisConfig.GetAddress(),
				redis.DialDatabase(redisConfig.GetDatabase()),
				redis.DialPassword(redisConfig.GetPassword()),
			)
		},
		// 检查心跳
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}

			_, err := c.Do(CommandPing)
			return err
		},
	}

	r := &Redis{
		pool: pool,
	}

	return r
}

/*
Set 默认Redis写入
  args:
   key 键
   val 值
   second 超时秒数, 仅接受1位参数

  return:
   error
*/
func Set(key string, val interface{}, second ...int) error {
	return defaultRedis.Set(key, val, second...)
}

/*
GetInt 默认Redis读取
  args:
   key 键

  return:
   int
   error
*/
func GetInt(key string) (int, error) {
	return defaultRedis.GetInt(key)
}

/*
GetInt64 默认Redis读取
  args:
   key 键

  return:
   int64
   error
*/
func GetInt64(key string) (int64, error) {
	return defaultRedis.GetInt64(key)
}

/*
GetUint64 默认Redis读取
  args:
   key 键

  return:
   unit64
   error
*/
func GetUint64(key string) (uint64, error) {
	return defaultRedis.GetUint64(key)
}

/*
GetFloat64 默认Redis读取
  args:
   key 键

  return:
   float64
   error
*/
func GetFloat64(key string) (float64, error) {
	return defaultRedis.GetFloat64(key)
}

/*
GetString 默认Redis读取
  args:
   key 键

  return:
   string
   error
*/
func GetString(key string) (string, error) {
	return defaultRedis.GetString(key)
}

/*
GetBytes 默认Redis读取
  args:
   key 键

  return:
   []byte
   error
*/
func GetBytes(key string) ([]byte, error) {
	return defaultRedis.GetBytes(key)
}

/*
GetBool 默认Redis读取
  args:
   key 键

  return:
   bool
   error
*/
func GetBool(key string) (bool, error) {
	return defaultRedis.GetBool(key)
}

/*
GetInterfaces 默认Redis读取
  args:
   key 键

  return:
   []interface{}
   error
*/
func GetInterfaces(key string) ([]interface{}, error) {
	return defaultRedis.GetInterfaces(key)
}

/*
GetFloat64s 默认Redis读取
  args:
   key 键

  return:
   []float64
   error
*/
func GetFloat64s(key string) ([]float64, error) {
	return defaultRedis.GetFloat64s(key)
}

/*
GetStrings 默认Redis读取
  args:
   key 键

  return:
   []string
   error
*/
func GetStrings(key string) ([]string, error) {
	return defaultRedis.GetStrings(key)
}

/*
GetByteSlices 默认Redis读取
  args:
   key 键

  return:
   [][]byte
   error
*/
func GetByteSlices(key string) ([][]byte, error) {
	return defaultRedis.GetByteSlices(key)
}

/*
GetInt64s 默认Redis读取
  args:
   key 键

  return:
   []int64
   error
*/
func GetInt64s(key string) ([]int64, error) {
	return defaultRedis.GetInt64s(key)
}

/*
GetInts 默认Redis读取
  args:
   key 键

  return:
   []int
   error
*/
func GetInts(key string) ([]int, error) {
	return defaultRedis.GetInts(key)
}

/*
GetUint64s 默认Redis读取
  args:
   key 键

  return:
   []uint64
   error
*/
func GetUint64s(key string) ([]uint64, error) {
	return defaultRedis.GetUint64s(key)
}

/*
GetStringMap 默认Redis读取
  args:
   key 键

  return:
   map[string]string
   error
*/
func GetStringMap(key string) (map[string]string, error) {
	return defaultRedis.GetStringMap(key)
}

/*
GetIntMap 默认Redis读取
  args:
   key 键

  return:
   map[string]int
   error
*/
func GetIntMap(key string) (map[string]int, error) {
	return defaultRedis.GetIntMap(key)
}

/*
GetInt64Map 默认Redis读取
  args:
   key 键

  return:
   map[string]int64
   error
*/
func GetInt64Map(key string) (map[string]int64, error) {
	return defaultRedis.GetInt64Map(key)
}

/*
GetUint64Map 默认Redis读取
  args:
   key 键

  return:
   map[string]uint64
   error
*/
func GetUint64Map(key string) (map[string]uint64, error) {
	return defaultRedis.GetUint64Map(key)
}

/*
GetPositions 默认Redis读取
  args:
   key 键

  return:
   []*[2]float64
   error
*/
func GetPositions(key string) ([]*[2]float64, error) {
	return defaultRedis.GetPositions(key)
}

/*
Del 默认Redis删除
  args:
   key 键

  return:
   error
*/
func Del(key string) error {
	return defaultRedis.Del(key)
}

/*
Exist 默认Redis判断是否存在
  args:
   key 键

  return:
   bool
   error
*/
func Exist(key string) (bool, error) {
	return defaultRedis.Exist(key)
}

/*
Expire 默认Redis设置超时
  args:
   key 键
   second 超时秒数

  return:
   error
*/
func Expire(key string, second int) error {
	return defaultRedis.Expire(key, second)
}

/*
Incr 默认Redis递增
  args:
   key 键

  return:
   int 递增后的数值
   error
*/
func Incr(key string) (int, error) {
	return defaultRedis.Incr(key)
}

/*
Decr 默认Redis递减
  args:
   key 键

  return:
   int 递减后的数值
   error
*/
func Decr(key string) (int, error) {
	return defaultRedis.Decr(key)
}
