package redis

import (
	"github.com/gomodule/redigo/redis"
)

type Redis struct {
	pool *redis.Pool
}

/*
GetConn 获取Redis链接
  return:
   redis.Conn
*/
func (r *Redis) GetConn() redis.Conn {
	return r.pool.Get()
}

/*
Set 写入
  args:
   key 键
   val 值
   second 超时秒数, 仅接受1位参数

  return:
   error
*/
func (r *Redis) Set(key string, val interface{}, second ...int) error {
	conn := r.pool.Get()
	defer conn.Close()

	var err error
	if nil != second {
		_, err = conn.Do(CommandSet, key, val, "EX", second[0])
	} else {
		_, err = conn.Do(CommandSet, key, val)
	}

	return err
}

/*
get 读取
  args:
   key 键

  return:
   interface{}
   error
*/
func (r *Redis) get(key string) (interface{}, error) {
	conn := r.pool.Get()
	defer conn.Close()
	return conn.Do(CommandGet, key)
}

/*
GetInt 读取
  args:
   key 键

  return:
   int
   error
*/
func (r *Redis) GetInt(key string) (int, error) {
	return redis.Int(r.get(key))
}

/*
GetInt64 读取
  args:
   key 键

  return:
   int64
   error
*/
func (r *Redis) GetInt64(key string) (int64, error) {
	return redis.Int64(r.get(key))
}

/*
GetUint64 读取
  args:
   key 键

  return:
   unit64
   error
*/
func (r *Redis) GetUint64(key string) (uint64, error) {
	return redis.Uint64(r.get(key))
}

/*
GetFloat64 读取
  args:
   key 键

  return:
   float64
   error
*/
func (r *Redis) GetFloat64(key string) (float64, error) {
	return redis.Float64(r.get(key))
}

/*
GetString 读取
  args:
   key 键

  return:
   string
   error
*/
func (r *Redis) GetString(key string) (string, error) {
	return redis.String(r.get(key))
}

/*
GetBytes 读取
  args:
   key 键

  return:
   []byte
   error
*/
func (r *Redis) GetBytes(key string) ([]byte, error) {
	return redis.Bytes(r.get(key))
}

/*
GetBool 读取
  args:
   key 键

  return:
   bool
   error
*/
func (r *Redis) GetBool(key string) (bool, error) {
	return redis.Bool(r.get(key))
}

/*
GetInterfaces 读取
  args:
   key 键

  return:
   []interface{}
   error
*/
func (r *Redis) GetInterfaces(key string) ([]interface{}, error) {
	return redis.Values(r.get(key))
}

/*
GetFloat64s 读取
  args:
   key 键

  return:
   []float64
   error
*/
func (r *Redis) GetFloat64s(key string) ([]float64, error) {
	return redis.Float64s(r.get(key))
}

/*
GetStrings 读取
  args:
   key 键

  return:
   []string
   error
*/
func (r *Redis) GetStrings(key string) ([]string, error) {
	return redis.Strings(r.get(key))
}

/*
GetByteSlices 读取
  args:
   key 键

  return:
   [][]byte
   error
*/
func (r *Redis) GetByteSlices(key string) ([][]byte, error) {
	return redis.ByteSlices(r.get(key))
}

/*
GetInt64s 读取
  args:
   key 键

  return:
   []int64
   error
*/
func (r *Redis) GetInt64s(key string) ([]int64, error) {
	return redis.Int64s(r.get(key))
}

/*
GetInts 读取
  args:
   key 键

  return:
   []int
   error
*/
func (r *Redis) GetInts(key string) ([]int, error) {
	return redis.Ints(r.get(key))
}

/*
GetUint64s 读取
  args:
   key 键

  return:
   []uint64
   error
*/
func (r *Redis) GetUint64s(key string) ([]uint64, error) {
	return redis.Uint64s(r.get(key))
}

/*
GetStringMap 读取
  args:
   key 键

  return:
   map[string]string
   error
*/
func (r *Redis) GetStringMap(key string) (map[string]string, error) {
	return redis.StringMap(r.get(key))
}

/*
GetIntMap 读取
  args:
   key 键

  return:
   map[string]int
   error
*/
func (r *Redis) GetIntMap(key string) (map[string]int, error) {
	return redis.IntMap(r.get(key))
}

/*
GetInt64Map 读取
  args:
   key 键

  return:
   map[string]int64
   error
*/
func (r *Redis) GetInt64Map(key string) (map[string]int64, error) {
	return redis.Int64Map(r.get(key))
}

/*
GetUint64Map 读取
  args:
   key 键

  return:
   map[string]uint64
   error
*/
func (r *Redis) GetUint64Map(key string) (map[string]uint64, error) {
	return redis.Uint64Map(r.get(key))
}

/*
GetPositions 读取
  args:
   key 键

  return:
   []*[2]float64
   error
*/
func (r *Redis) GetPositions(key string) ([]*[2]float64, error) {
	return redis.Positions(r.get(key))
}

/*
Del 删除
  args:
   key 键

  return:
   error
*/
func (r *Redis) Del(key string) error {
	conn := r.pool.Get()
	defer conn.Close()
	_, err := conn.Do(CommandDel, key)
	return err
}

/*
Exist 判断是否存在
  args:
   key 键

  return:
   bool
   error
*/
func (r *Redis) Exist(key string) (bool, error) {
	conn := r.pool.Get()
	defer conn.Close()
	return redis.Bool(conn.Do(CommandExist, key))
}

/*
Expire 设置超时
  args:
   key 键
   second 超时秒数

  return:
   error
*/
func (r *Redis) Expire(key string, second int) error {
	conn := r.pool.Get()
	defer conn.Close()
	_, err := conn.Do(CommandExpire, key, second)
	return err
}

/*
Incr 递增
  args:
   key 键

  return:
   int 递增后的数值
   error
*/
func (r *Redis) Incr(key string) (int, error) {
	conn := r.pool.Get()
	defer conn.Close()
	return redis.Int(conn.Do(CommandIncr, key))
}

/*
Decr 递减
  args:
   key 键

  return:
   int 递减后的数值
   error
*/
func (r *Redis) Decr(key string) (int, error) {
	conn := r.pool.Get()
	defer conn.Close()
	return redis.Int(conn.Do(CommandDecr, key))
}
