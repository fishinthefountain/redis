package redis

const (
	// CommandGet Get命令
	CommandGet = "GET"
	// CommandSet Set命令
	CommandSet = "SET"
	// CommandDel Del命令
	CommandDel = "DEL"
	// CommandExist Exist命令
	CommandExist = "EXISTS"
	// CommandExpire Expire命令
	CommandExpire = "EXPIRE"
	// CommandPing Ping命令
	CommandPing = "PING"
	// CommandIncr Incr命令
	CommandIncr = "INCR"
	// CommandDecr Decr命令
	CommandDecr = "DECR"
)

// IRedisConfig Redis配置接口
type IRedisConfig interface {
	// GetWait 超出最大连接数时是否等待, 不等待则报错
	GetWait() bool
	// GetNetWork 链接类型, tcp
	GetNetWork() string
	// GetAddress 主机地址
	GetAddress() string
	// GetDatabase 数据库
	GetDatabase() int
	// GetPassword 密码
	GetPassword() string
	// GetMaxIdle 最大闲置链接数
	GetMaxIdle() int
	// GetMaxActive 最大可用链接数
	GetMaxActive() int
	// GetIdleTimeout 闲置超时分钟
	GetIdleTimeout() int64
}
