package redis

import (
	"errors"
	"fmt"
	"testing"
	"time"
)

type redisConfig struct {
	Wait        bool
	NetWork     string
	Address     string
	Database    int
	Password    string
	MaxIdle     int
	MaxActive   int
	IdleTimeout int64
}

func (r redisConfig) GetWait() bool {
	return r.Wait
}

func (r redisConfig) GetNetWork() string {
	return r.NetWork
}

func (r redisConfig) GetAddress() string {
	return r.Address
}

func (r redisConfig) GetDatabase() int {
	return r.Database
}

func (r redisConfig) GetPassword() string {
	return r.Password
}

func (r redisConfig) GetMaxIdle() int {
	return r.MaxIdle
}

func (r redisConfig) GetMaxActive() int {
	return r.MaxActive
}

func (r redisConfig) GetIdleTimeout() int64 {
	return r.IdleTimeout
}

var config = &redisConfig{
	Wait: true,
	NetWork: "tcp",
	Address: "127.0.0.1:6379",
	Database: 0,
	Password: "",
	MaxIdle: 1,
	MaxActive: 20,
	IdleTimeout: 5,
}

func set(r *Redis, key string, val interface{}, second ...int) {
	if err := r.Set(key, val, second...); nil != err {
		panic(err)
	}
}

func TestSet(t *testing.T) {
	r := New(config)

	var key string = "test_key_1"
	var val interface{} = "test_val_1"

	set(r, key, val)

	exist, err := r.Exist(key)
	if nil != err {
		panic(err)
	}

	if false == exist {
		panic(errors.New("lose key"))
	}
}

func TestGet(t *testing.T) {
	r := New(config)

	var key string = "test_key_1"
	var val string = "test_val_1"
	set(r, key, val)

	getVal, err := r.GetString(key)
	if nil != err {
		panic(err)
	}

	if val != getVal {
		panic(fmt.Errorf("val is not match, val: %s getVal: %s", val, getVal))
	}
}

func TestExpire(t *testing.T) {
	r := New(config)

	key := "test_key_1"
	val := "test_val_1"
	second := 5
	set(r, key, val)

	if err := r.Expire(key, second); nil != err {
		panic(err)
	}

	time.Sleep(5500 * time.Millisecond)

	exist, err := r.Exist(key)
	if nil != err {
		panic(err)
	}

	if exist {
		panic(fmt.Errorf("%s is expired but also exist", key))
	}
}

func TestDel(t *testing.T) {
	TestSet(t)

	r := New(config)
	key := "test_key_1"
	val := "test_val_1"
	set(r, key, val)

	if err := r.Del(key); nil != err {
		panic(err)
	}

	exist, err := r.Exist(key)
	if nil != err {
		panic(err)
	}

	if exist {
		panic(fmt.Errorf("%s is deleted but also exist", key))
	}
}

func TestIncr(t *testing.T) {
	r := New(config)

	key := "test_key_1"
	val := 1
	set(r, key, val)

	i, err := r.Incr(key)
	if nil != err {
		panic(err)
	}

	t.Log("Incr num:", i)
	if 2 != i {
		panic(fmt.Errorf("val incr fail, old: %v new: %v", val, i))
	}

	i, err = r.GetInt(key)
	if nil != err {
		panic(err)
	}

	t.Log("Incr num:", i)
	if 2 != i {
		panic(fmt.Errorf("val incr fail, old: %v new: %v", val, i))
	}
}

func TestDecr(t *testing.T) {
	r := New(config)

	key := "test_key_1"
	val := 2
	t.Log("Decr num:", val)

	set(r, key, val)

	i, err := r.Decr(key)
	if nil != err {
		panic(err)
	}

	t.Log("Decr num:", i)
	if 1 != i {
		panic(fmt.Errorf("val decr fail, old: %v new: %v", val, i))
	}

	i, err = r.GetInt(key)
	if nil != err {
		panic(err)
	}

	t.Log("Decr num:", i)
	if 1 != i {
		panic(fmt.Errorf("val decr fail, old: %v new: %v", val, i))
	}
}
