package redis

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"
)

func utilSet(key string, val interface{}, second ...int) {
	if err := Set(key, val, second...); nil != err {
		panic(err)
	}
}

func TestUtilSet(t *testing.T) {
	Init(config)

	var key string = "test_key_1"
	var val interface{} = "test_val_1"

	utilSet(key, val)

	exist, err := Exist(key)
	if nil != err {
		panic(err)
	}

	if false == exist {
		panic(errors.New("lose key"))
	}
}

func TestUtilGet(t *testing.T) {
	Init(config)

	var key string = "test_key_1"
	var val string = "test_val_1"

	utilSet(key, val)

	getVal, err := GetString(key)
	if nil != err {
		panic(err)
	}

	if reflect.ValueOf(val).Interface() != reflect.ValueOf(getVal).Interface() {
		panic(fmt.Errorf("val is not match, val: %s getVal: %s", val, getVal))
	}
}

func TestUtilExpire(t *testing.T) {
	Init(config)

	key := "test_key_1"
	val := "test_val_1"
	second := 5
	utilSet(key, val)

	if err := Expire(key, second); nil != err {
		panic(err)
	}

	time.Sleep(5500 * time.Millisecond)

	exist, err := Exist(key)
	if nil != err {
		panic(err)
	}

	if exist {
		panic(fmt.Errorf("%s is expired but also exist", key))
	}
}

func TestUtilDel(t *testing.T) {
	Init(config)

	key := "test_key_1"
	val := "test_val_1"
	utilSet(key, val)

	if err := Del(key); nil != err {
		panic(err)
	}

	exist, err := Exist(key)
	if nil != err {
		panic(err)
	}

	if exist {
		panic(fmt.Errorf("%s is deleted but also exist", key))
	}
}

func TestUtilIncr(t *testing.T) {
	Init(config)

	key := "test_key_1"
	val := 1
	t.Log("Incr num:", val)
	utilSet(key, val)

	i, err := Incr(key)
	if nil != err {
		panic(err)
	}

	t.Log("Incr num:", i)
	if 2 != i {
		panic(fmt.Errorf("val incr fail, old: %v new: %v", val, i))
	}

	i, err = GetInt(key)
	if nil != err {
		panic(err)
	}

	t.Log("Incr num:", i)
	if 2 != i {
		panic(fmt.Errorf("val incr fail, old: %v new: %v", val, i))
	}
}

func TestUtilDecr(t *testing.T) {
	Init(config)

	key := "test_key_1"
	val := 2
	t.Log("Decr num:", val)
	utilSet(key, val)

	i, err := Decr(key)
	if nil != err {
		panic(err)
	}

	t.Log("Decr num:", i)
	if 1 != i {
		panic(fmt.Errorf("val decr fail, old: %v new: %v", val, i))
	}

	i, err = GetInt(key)
	if nil != err {
		panic(err)
	}

	t.Log("Decr num:", i)
	if 1 != i {
		panic(fmt.Errorf("val decr fail, old: %v new: %v", val, i))
	}
}
